﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.iOS;

namespace ARVRLab.ARBarcode.ARKit
{
    /// <summary>
    /// Реализация рендеринга кадра камеры ARKit в Texture2D
    /// </summary>  
    public class ARKitFrameRender
    {
        public readonly UnityARVideo arVideo;
        private readonly int width, height;

        private RenderTexture renderPortrait, renderLandscape;
        private Texture2D texturePortrait, textureLandscape;

        public ARKitFrameRender(UnityARVideo arVideo, int width, int height)
        {
            this.arVideo = arVideo;
            this.width = width;
            this.height = height;

            InitTextures();
        }

        private void InitTextures()
        {
            renderLandscape = new RenderTexture(width, height, 0);
            renderPortrait = new RenderTexture(height, width, 0);

            textureLandscape = new Texture2D(renderLandscape.width, renderLandscape.height, TextureFormat.RGB24, false);
            texturePortrait = new Texture2D(renderPortrait.width, renderPortrait.height, TextureFormat.RGB24, false);
        }

        /// <summary>
        /// Рендерит текущий кадр в текстуру
        /// Не рекомендую вызывать в Update - приводит к падению FPS
        /// </summary>
        /// <returns></returns>
        public Texture2D RenderCameraTexture()
        {
            var currentRender = Screen.orientation == ScreenOrientation.Landscape ? renderLandscape : renderPortrait;
            if (currentRender == null)
                return null;

            Graphics.Blit(null, currentRender, arVideo.m_ClearMaterial);

            var currentTexture = Screen.orientation == ScreenOrientation.Landscape ? textureLandscape : texturePortrait;

            RenderTexture.active = currentRender;
            currentTexture.ReadPixels(new Rect(0, 0, currentRender.width, currentRender.height), 0, 0);
            currentTexture.Apply();

            return currentTexture;
        }
    }
}