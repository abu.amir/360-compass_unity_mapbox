﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoffeeStateChanger : MonoBehaviour
{
    [SerializeField]
    private  GameObject ApproveState;
    [SerializeField]
    private GameObject Coffee;
    [SerializeField]
    private GameObject BuyBtn;

    private Animator animator;

    public static CoffeeStateChanger instance;


    void Awake(){
        ApproveState.SetActive(false);
        animator = ApproveState.GetComponent<Animator>();
        instance = this;
    }


    public void BuyBtnPressed(){

        Debug.Log("BuyBtnPressed");
        ApproveState.SetActive(true);

    }

    public void ApprovedBtnPressed(){
        animator.SetTrigger("Falling");
        Coffee.SetActive(false);
        BuyBtn.SetActive(false);
    }
}
