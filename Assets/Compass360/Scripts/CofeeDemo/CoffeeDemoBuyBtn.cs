using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoffeeDemoBuyBtn : MonoBehaviour
{
    CoffeeStateChanger stateChanger;

    // Start is called before the first frame update
   
    void Start()
    {
        stateChanger = CoffeeStateChanger.instance;
        if (stateChanger == null)
            Debug.LogWarning("There isn't entity of Coffee State Changer");
    }

    private void OnMouseDown()
    {
        Debug.Log("OnMouseDown");
        stateChanger.BuyBtnPressed();
    }
}
