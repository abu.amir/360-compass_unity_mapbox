﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public Text text;
    public Transform map;

    int rotation = 0;

    public void Rotate(){
        rotation += 90;
        if(rotation > 180){
            rotation -= 360;
        }
        text.text = rotation + "";
        map.Rotate(0, 90, 0);
    }
}
