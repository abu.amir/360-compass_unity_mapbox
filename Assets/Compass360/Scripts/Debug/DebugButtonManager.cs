using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugButtonManager : MonoBehaviour
{


    [SerializeField]
    private GameObject DebugCanvas;

    [HideInInspector]
    public bool CurrentState = false;

    // Start is called before the first frame update
    void Start()
    {
        DebugCanvas.SetActive(CurrentState);
    }

    public void OnClickDebugBtn(){
        CurrentState = !CurrentState;
        DebugCanvas.SetActive(CurrentState);
    }
}
