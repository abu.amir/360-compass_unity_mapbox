using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour
{
    [SerializeField]
    Material managedMatrial;

    [SerializeField]
    Color disabledColor;
    [SerializeField]
    Color enabledColor;

    // Start is called before the first frame update
    void Start()
    {
        managedMatrial.color = disabledColor;
    }

    public void onValueChanged(bool value){
        if (value) {
            managedMatrial.color = enabledColor;
        }else{
            managedMatrial.color = disabledColor;
        }
    }

    // Update is called once per frame
    void Update()
    { }
}
