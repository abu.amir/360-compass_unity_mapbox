using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class LocalDebugSystem : MonoBehaviour
{

    public static LocalDebugSystem Instance;

    [SerializeField]
    public Text DebugText;

    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }

    public void ClearDebugWindow()
    {
        DebugText.text = "";
    }

    public static void Log (string s){
        Instance.DebugText.text += System.Environment.NewLine;
        Instance.DebugText.text += s;
    }
}
