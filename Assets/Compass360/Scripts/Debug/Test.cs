using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public MapManager mapManager;

    public static event System.Action<int> OnTestHappend;

    private void Awake()
    {
        OnTestHappend += Test_OnTestHappend;
        OnTestHappend -= Test_OnTestHappend;
      //  Test.OnTestHappend
    }


    private void Update()
    {
        if (Time.time >= 2)
        {
            if (OnTestHappend != null)
                OnTestHappend((int) Time.time);

            //OnTestHappend?.Invoke((int)Time.time);
        }
    }
    // Start is called before the first frame update
    //void Start()
    //{
    //    TestDelegate func = RandomFunction;
    //    func(10);


    //}

    //private void RandomFunction(int foo)
    //{
    //    throw new NotImplementedException();
    //}

    void Test_OnTestHappend(int foo)
    {
        print(foo);
    }

}
