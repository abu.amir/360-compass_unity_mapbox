using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{

    [SerializeField]
    private Transform map;

    public Proxy proxy;

    float smooth = 5.0f;

    public void Start()
    {
        Proxy.Instance.OnGetDegree += getDegree;
        Proxy.Instance.OnSend += RevokeSimulation;
        map = gameObject.transform;
    }

    void getDegree(int value){
        Debug.Log("I got degree wich is " + value);
  
        //Quaternion rotator = Quaternion.Euler(0, value, 0);
        map.Rotate(0, -value, 0);
    }

    void RevokeSimulation(){
        transform.rotation = Quaternion.Euler(0, Camera.main.transform.rotation.eulerAngles.y, 0); //Rotation wich simulating revoking players's rotation, althought that's the world's round him rotation
    }


}
