using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Compass360 : MonoBehaviour
{
    [SerializeField]
    private Text logText;
    [SerializeField]
    private bool useText = false;
    [SerializeField]
    private Proxy proxy;

    private void Update()
    {
        proxy.OnGetDegree += SetRotation;
    }

    public enum Direction { x, y, z }

    [SerializeField]
    private Direction direction = Direction.x;

    public void SetRotation(int value)
    {
        //transform.rotation = Quaternion.Euler(0, 0, Input.compass.trueHeading);
        //Debug.Log("Set Rotation");

        if (direction == Direction.x)
        {
            transform.rotation = Quaternion.Euler(-value, 0, 0);
        }

        if (direction == Direction.y)
        {
            transform.rotation = Quaternion.Euler(0, -value, 0);
        }
        if (direction == Direction.z)
        {
            transform.rotation = Quaternion.Euler(0, 0, value);
        }

    }
}
