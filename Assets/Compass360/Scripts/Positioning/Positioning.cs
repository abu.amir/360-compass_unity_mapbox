using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Positioning : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        Proxy.Instance.OnGetPosition += OnGetPositionHandler;
    }

    private Vector3 deltaVector;


    void OnGetPositionHandler(Vector3 _deltaVector){

        deltaVector = _deltaVector;
    }

    public void OnPosBtnClick(){

        LocalDebugSystem.Log("Pos bef: " + transform.position);
        LocalDebugSystem.Log("Vector: " + deltaVector);
        transform.localPosition += deltaVector;
        LocalDebugSystem.Log("Pos after: " + transform.position);

    }
}
