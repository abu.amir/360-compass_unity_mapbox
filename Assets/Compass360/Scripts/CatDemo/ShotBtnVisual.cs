using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShotBtnVisual : MonoBehaviour
{

    private Image image;
    public GameObject btn;

    private Proxy proxy;

    private bool run = false;

    private void Start()
    {
        image = GetComponent<Image>();
        proxy = Proxy.Instance;
        proxy.OnGetDegree += Finish;
    }

    private void Update()
    {
        if(Input.GetKey(KeyCode.A)){
            Finish(0);
        }
    }

    public void OnClick(){
        StartCoroutine(StartSpinning());
        btn.transform.localScale = new Vector3(0.8f, 0.8f, 1);
    }

    public void Finish(int a){
        image.fillAmount = 1f;
        btn.transform.localScale = new Vector3(1f, 1f, 1);
        StopAllCoroutines();
    }

 
    protected float maxValue = 2f, minValue = 0f;

    // Create a property to handle the slider's value
    private float currentValue = 0f;
    public float CurrentValue
    {
        get
        {
            return currentValue;
        }
        set
        {
            // Ensure the passed value falls within min/max range
            currentValue = Mathf.Clamp(value, minValue, maxValue);

            // Calculate the current fill percentage and display it
            float fillPercentage = currentValue / maxValue;
            image.fillAmount = fillPercentage;
        }
    }

    IEnumerator StartSpinning()
    {
        image.fillAmount = 0.2f;
        while (true)
        {
            image.rectTransform.Rotate(new Vector3(0, 0, 1), 12f);
            yield return new WaitForSeconds(0.03f);
        }
    }

    // Update is called once per frame

}
