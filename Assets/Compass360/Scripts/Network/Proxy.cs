using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine;


public class Proxy : MonoBehaviour
{

    public static Proxy Instance;
    //Events
    public event System.Action<int> OnGetDegree; //Invoked, when server gives you a feedback abiut degree
    public event System.Action<Vector3> OnGetPosition; //Invoked, when server gives you a feedback about degree
    public event System.Action OnSend; //Invoked, when we send request to server


    [Header("UI elements")]
    //UI and IP adress issues
    [SerializeField]
    private Text compassBtnText; //Text on compass Button
    [SerializeField]
    private Text posBtnText; //Text on pos Button
    [SerializeField]
    private Text ipAdressText; //Field with ip adress wich may be setted up by user
    [SerializeField]
    private Image indicator;

    [Header("Server IP")]
    public String ReserveServerIP = "http://18ae7f4c.ngrok.io/"; //Reserve IP adress

    [Header("Checking")]
    [SerializeField]
    private float checkDelayTime = 5f;

    private const String upload = "upload";
    private const String check = "check";

    //Result stuff
    private const String serverAvailable = "ServerAvailable";
    private const String badResult = "FAIL";
    private float ResultDegree;



    private void Awake()
    {
        Instance = this;

    }
    //Checking routine indicator
    private void Start()
    {
        InvokeRepeating("CheckRoutine", 1.0f, 5.0f);

        ReserveServerIP = PlayerPrefs.GetString("ip_adress");

    }


    public void makeRequest(byte[] bytesOfImage)
    {

        OnSend?.Invoke();

        compassBtnText.text = "Waiting";
        StartCoroutine(Upload(bytesOfImage));

    }

    //Coroutine which loads Image to the server and reacts to requests
    IEnumerator Upload(byte[] bytesOfImage)
    {

        string fileName = GetFileName();

        WWWForm form = new WWWForm();

        float latitude = GPS.Instance.latitude;
        float longitude = GPS.Instance.longitude;
        float horizontalAccuracy = GPS.Instance.horizontalAccuracy;

        Vector3 accelerationVector = Input.acceleration;
        Quaternion gyroQuaternion = Input.gyro.attitude;


        form.AddField("latitude", latitude.ToString());
        form.AddField("longitude", longitude.ToString());
        form.AddField("horizontalAccuracy", horizontalAccuracy.ToString());
        form.AddField("accelerationVector", accelerationVector.ToString());
        form.AddField("gyroQuaternion", gyroQuaternion.ToString());


        form.AddField("image_file", "file");
        form.AddBinaryData("image_file", bytesOfImage, fileName);


        var ip = GetIP() + upload;
        var www = UnityWebRequest.Post(ip, form);



        yield return www.SendWebRequest();

        try
        {
            String result = www.downloadHandler.text;
            Debug.Log("Result = " + www.downloadHandler.text);
            //ResultDegree = float.Parse(www.downloadHandler.text);

            processResult(result);

            if (ipAdressText.text != "") { PlayerPrefs.SetString("ip_adress", ipAdressText.text); } //Adding the value to the player prefs
            //Check if it's a bad request (Fail)
            //if (!result.Equals(badResult))
            //{
            //    //If it's the good result request
            //    OnGetDegree?.Invoke((int)float.Parse(result));
            //}

            ////
            //uitext.text = result;


        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
        }



    }

    //The Method managing real and reserve IP adresses
    private String GetIP()
    {

        if (ipAdressText.text == "")
        {
            return ReserveServerIP;
        }
        return ipAdressText.text;
    }

    //The Method formats file name
    private string GetFileName()
    {
        string file = "";
        DateTime dateTime = DateTime.Now;
        file = dateTime.ToString("MM.dd.yyyy_HH:mm:ss");
        //file += ".png";
        //Debug.Log("fileName = " + file);
        return file;
    }



    private void CheckRoutine()
    {
        StartCoroutine(Check());
    }

    //Routine of checking is Server Available
    private IEnumerator Check()
    {

        WWWForm form = new WWWForm();
        var ip = GetIP() + check;

        Debug.Log("Check uses ip=" + ip);

        var www = UnityWebRequest.Post(ip, form);

        yield return www.SendWebRequest();

        try
        {
            String result = www.downloadHandler.text;
            Debug.Log("Check result = " + www.downloadHandler.text);

            //Check if it's a bad request (Fail)
            if (result.Equals(serverAvailable))
            {

                Debug.Log("SERVER WORKS");
                //If it's the server available
                indicator.color = Color.green;
            }
            else
            {
                //If it's the server unavailable
                Debug.Log("SERVER DOESN'T WORK");
                indicator.color = Color.red;
            }

        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
            indicator.color = Color.red;
        }

    }

    private void processResult(String jsonString)
    {


        //jsonString = { "name": "07.16.2019_10:56:38", "time": 10.185722827911377, "result": "SECCESS", "error": "-", "matches": 0, "degree": -48.43421052631578, "ransac": 1.938988545436348, "position": ["55.8019", "37.54774"], "confidence": 9.908018143163789, "panorama": "0005.JPG"}
        RequestedData requestedData = JsonUtility.FromJson<RequestedData>(jsonString);



        //Check if it's a bad request (Fail)
        if (!requestedData.result.Equals(badResult))
        {
            //If it's the good result request
            LocalDebugSystem.Log("Confidence: " + requestedData.confidence);

            //Preparing shift vector
            float xValue = float.Parse(requestedData.shift_vector[0], System.Globalization.CultureInfo.InvariantCulture);
            float zValue = float.Parse(requestedData.shift_vector[1], System.Globalization.CultureInfo.InvariantCulture);
            Vector3 shiftVector = new Vector3(xValue, 0, zValue);
            //Invoking OnGetPosition degree
            OnGetPosition?.Invoke(-shiftVector);

            //Invoking OnGetDegree event, checking if a degree value is float than round to int 
            OnGetDegree?.Invoke(Mathf.RoundToInt(requestedData.degree));

            compassBtnText.text = requestedData.degree.ToString();
            posBtnText.text = requestedData.shift_vector[0] + " " + requestedData.shift_vector[1];
        }
        else{
            compassBtnText.text = requestedData.result;
            posBtnText.text = requestedData.result;
        }

    } 
}


[Serializable]
class RequestedData
{
    public string name;
    public float time;
    public string result;
    public string error;
    public int matches;
    public float degree;
    public List<String> position;
    public float confidence;
    public string panorama;
    public List<String> shift_vector;
}