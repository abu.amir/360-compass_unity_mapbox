﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenSpace : MonoBehaviour
{
    [SerializeField]
    private float scale = 5.0f;

    private Transform goal;

    // Start is called before the first frame update
    void Start()
    {
        goal = Camera.main.transform;        
    }

    // Update is called once per frame
    void Update()
    {
        var dist = Vector3.Distance(transform.position, goal.position);
        transform.localScale = Vector3.one * dist * scale;
    }
}
