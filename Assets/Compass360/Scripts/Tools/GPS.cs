﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GPS : MonoBehaviour
{

    public static GPS Instance { set; get; }

    [HideInInspector]
    public float latitude;
    [HideInInspector]
    public float longitude;
    [HideInInspector]
    public float horizontalAccuracy;

    private void Start()
    {

        // Start service before querying location
        Input.location.Start();

        Instance = this;
        DontDestroyOnLoad(gameObject);
        InvokeRepeating("UpdateGPS", 0.1f, 20.0f);
    }

    public void UpdateGPS(){
        StartCoroutine(GetLocation());
    }


    IEnumerator GetLocation()
    {
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
        {
            Debug.Log( "Location service is unavailable on the device");
            yield break;
        }




        Debug.Log("Location Service started");
        // Wait until service initializes
        int maxWait = 20; 
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            //text.text += (" LocationServiceStatus " + Input.location.status.ToString());
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            Debug.Log("Timed out, now LocationServiceStatus is " + Input.location.status.ToString());
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            Debug.Log("Unable to determine device location");
            yield break;
        }
        else
        {
            // Access granted and location value could be retrieved
            latitude = Input.location.lastData.latitude;
            longitude = Input.location.lastData.longitude;
            horizontalAccuracy = Input.location.lastData.horizontalAccuracy;

            Debug.Log("Location: " + latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);

        }

        // Stop service if there is no need to query location updates continuously
        //Input.location.Stop()

        Debug.Log("Coordiantes updated. Latitude " + latitude + " longitude " + longitude);
    }

    public string Convert2Text(){
        return "" + latitude + " " + longitude;
    }
}